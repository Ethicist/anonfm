# Anon.FM Player

![License badge](https://raster.shields.io/badge/license-The%20Unlicense-lightgrey.png "License badge")

![Alt text](thumbnail.png?raw=true "Thumbnail")

## Demo

See in action on [GitLab pages](https://ethicist.gitlab.io/anonfm) or [here](https://anonfm.surge.sh/)

## Contributing

Please feel free to submit pull requests!
Bugfixes and simple non-breaking improvements will be accepted without any questions.

## License

This is free and unencumbered software released into the public domain.  
For more information, please refer to the [LICENSE](LICENSE) file or [unlicense.org](https://unlicense.org).
