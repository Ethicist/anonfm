/* * * * *
 * Title
 * * * * */

    var title = [
        "Рефлексивное","Аутичное","Чистое","Дискретность мышления",
        "Когнитивное","Картавое","На самом интересном месте",
        "Интровертное","На семи семплах","Рекурсивное","Утренний израиль",
        "Поехавших людей","DDoS","Ночные гадости","Кокаинум","Омич",
        "Анонимус","Упоротый школьник","Школонимус","Уже не торт",
        "Говно","Хуита","Чан","Радио","Веобу","Патлота","Пиздолизное",
        "Жополизное","Неймфажеское","Анонимус такой анонимный","ВЫКЛЮЧИЛ",
        "Хуесосное","Джазфажное","Электронное","Тёпло-ламповое",
        "Брутально-бессердечное","Популярное","Как бы","Шароварное",
        "Вячеславное","Файтдапавное","Проект любимого человека",
        "MPD","Прыщеблядское","Вендоблядское","Радонеж",
        "Фейл","Вин","Уныние","Чёткая волна","Гей-кружок","Быдло",
        "Ебаный стыд","бубубубу","Успешное","Хуже пидараса",
        "Разрывное","Деревня","Кто ты","Огни Большого Бваны",
        "Unspecified description","Неуловимое","Butthurt.fm",
        "Сарафанное","Фимоз","Аматорное","Раковые шейки",
        "Тирачевское","Понтовое","Озабоченное","Интерпрайз",
        "FreeBSD","LOVE :*","Настоящее","Которое не хочется выключать",
        "Segmentation fault","Сыктывкарская Правда. Мы доставляем"
    ];

    document.title = "Радио «" + title[Math.floor(Math.random()*title.length)] + "»";

/* * * * * *
 * Pseudo-EQ
 * * * * * */
 
    /*
    var i = 0;
    var flag = 0;
    var message = [
        "▁▂▃▄▅▆▇",
        "▂▁▂▃▄▅▆",
        "▃▂▁▂▃▄▅",
        "▄▃▂▁▂▃▄",
        "▅▄▃▂▁▂▃",
        "▆▅▄▃▂▁▂",
        "▇▆▅▄▃▂▁",
        "▆▇▆▅▄▃▂",
        "▅▆▇▆▅▄▃",
        "▄▅▆▇▆▅▄",
        "▃▄▅▆▇▆▅",
        "▂▃▄▅▆▇▆"
    ];


    function switch_animation(state) {
        flag = state ? true : false;    
    }

    function animate(){
      if (flag) {
          if (i >= message.length-1) {
              i = 0;
          } else {
              document.getElementById('eq').innerHTML = message[i];
            // document.getElementById('eq').innerHTML = message[Math.floor(Math.random() * message.length) ];
              i++;
          }
      }
    }

    setInterval(animate, 128);
    switch_animation(false);

    */

/* * * * *
 * Status
 * * * * */

    var sid = 0;

    function update() {
        document.getElementById('status').src = "https://anon.fm/radioanon.png?" + (sid++ % 200);
    }

    setInterval(update, 10000);
    update();

/* * * * * * * *
 * Play / Pause
 * * * * * * * */
 
    var player = document.getElementById('player');
    var source = document.getElementById('source');

    var is_playing = false;

    function switch_state(state){
        if (state == true) {
            document.getElementById('play').style.display = "none";
            document.getElementById('pause').style.display = "block";
        } else {
            document.getElementById('play').style.display = "block";
            document.getElementById('pause').style.display = "none";
        }
        is_playing = state ? true : false;
    }

    switch_state(false);

    document.getElementById('play').onclick = function() {
        //player.load();
        player.play();
        switch_state(true);
        //switch_animation(true);
    }
    document.getElementById('pause').onclick = function() {
        player.pause();
        switch_state(false);
        //switch_animation(false);
    }
    
/* * * * * * * * * * * * *
 * Change source bitrate
 * * * * * * * * * * * * */
    
    var stream_source = [
        "https://anon.fm/streams/radio",
        "https://anon.fm/streams/radio-low",
        "http://anon.fm:8000/radio.aac",
        "https://anon.fm/streams/radio.ogg"
    ];
    
    list.onclick = function(e) {
        var target = e.target;
        var index = target.getAttribute('index');
        source.src = stream_source[index];
        
        for (var i=0; i<= 3; i++) {
            var choose = document.getElementsByClassName('stream')[i];
        
            if (choose.className.indexOf("active") != -1) {
                if (i != index) {
                    choose.classList.remove("active");
                }
            } else {
                if (i == index) {
                    choose.classList.add("active");
                }
            }
        }
        
        if (is_playing == true) {
            player.pause();
            switch_state(false);
        }
        
        player.load();
        player.play();
        switch_state(true);
    };
	
/* * * * *
 * Volume
 * * * * */
 
    var slider = document.getElementById("myRange");
	var volume = document.getElementsByClassName("volume_icon")[0];

	slider.addEventListener("input", function() { 
	   player.volume = slider.value/100; 
		if (slider.value < 2) {
			player.volume = 0;
			volume.style.background = "url('./assets/volume.png') 0px 0px";
		} else if (slider.value < 35 && slider.value >= 1) {
			volume.style.background = "url('./assets/volume.png') -32px 0px";
		} else if (slider.value < 70 && slider.value >= 35) {
			volume.style.background = "url('./assets/volume.png') 0px -32px";
		} else if (slider.value >= 70) {
			volume.style.background = "url('./assets/volume.png') -32px -32px";
		}
		//console.log(slider.value);
	}, false);
	
/* * * * * * *
 * New window
 * * * * * * */

	function nw(l){
		window.status = "chat"
		strfeatures = "top=400,left=250,width=560,height=256,toolbar=no"
		window.open("/"+l+"/","win1"+l,strfeatures)
	}
    
/* * * * * * *
 * Random pic
 * * * * * * */
 
    document.getElementById("picrandom").onclick = function() {
        var url = "https://anon.fm/de2/" + Math.round(Math.random()*509+1) + ".jpg";
        var w = window.open(url, '_blank');
        w.focus();
    }
    
/* * * * * * * * * * *
 * Toogle background
 * * * * * * * * * * */
    
    var bg_enabled = false;
    var bg_target = document.getElementsByClassName('background')[0];
    var bg_trig_1 = document.getElementById('enable_bg');
    var bg_trig_0 = document.getElementById('disable_bg');
    
    function switch_bg(state) {
        if (state == true) {
            bg_target.style.display = "block";
            bg_trig_1.style.display = "none";
            bg_trig_0.style.display = "block";
        } else {
            bg_target.style.display = "none";
            bg_trig_1.style.display = "block";
            bg_trig_0.style.display = "none";
        }
        bg_enabled = state ? true : false;
    }
    
    switch_bg(false);

    document.getElementById('enable_bg').onclick = function() {
        switch_bg(true);
    }
    document.getElementById('disable_bg').onclick = function() {
        switch_bg(false);
    }